package command

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func newRunCmd(cfg Config, buf *bytes.Buffer) cli.App {
	log.SetOutput(buf)
	app := cli.NewApp()
	app.Name = "run"
	app.Usage = "Test run command"
	app.Commands = []*cli.Command{Run(cfg)}

	return *app
}

func newAnalyzeFunc(err error) AnalyzeFunc {
	return AnalyzeFunc(func(ctx *cli.Context, target string) (io.ReadCloser, error) {
		return ioutil.NopCloser(bytes.NewBuffer([]byte(""))), err
	})
}

func newConvertFunc(err error) ConvertFunc {
	return ConvertFunc(func(input io.Reader, prependPath string) (*report.Report, error) {
		newReport := report.NewReport()

		return &newReport, err
	})
}

func newMatchFunc(err error) search.MatchFunc {
	return func(path string, info os.FileInfo) (bool, error) {
		return true, err
	}
}

func TestRunErrorsOnSubCommands(t *testing.T) {
	tcs := []struct {
		Name          string
		Analyze       AnalyzeFunc
		Convert       ConvertFunc
		Match         search.MatchFunc
		wantErr       bool
		wantLogLevel  string
		wantDetecting string
		wantFound     string
		wantRunning   string
		wantCreating  string
	}{
		{
			Analyze:       newAnalyzeFunc(nil),
			Convert:       newConvertFunc(nil),
			Match:         newMatchFunc(nil),
			wantErr:       false,
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Found project in",
			wantRunning:   "▶ Running analyzer",
			wantCreating:  "▶ Creating report",
		},
		{
			Analyze:       newAnalyzeFunc(errors.New("oh noez")),
			Convert:       newConvertFunc(nil),
			Match:         newMatchFunc(nil),
			wantErr:       true,
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Found project in",
			wantRunning:   "▶ Running analyzer",
		},
		{
			Analyze:       newAnalyzeFunc(nil),
			Convert:       newConvertFunc(errors.New("oh noez")),
			Match:         newMatchFunc(nil),
			wantErr:       true,
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Found project in",
			wantRunning:   "▶ Running analyzer",
		},
		{
			Analyze:       newAnalyzeFunc(nil),
			Convert:       newConvertFunc(nil),
			Match:         newMatchFunc(errors.New("oh noez")),
			wantErr:       true,
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Found project in",
			wantRunning:   "▶ Running analyzer",
		},
	}

	for idx := range tcs {
		tt := tcs[idx]

		t.Run(tt.Name, func(t *testing.T) {
			cfg := Config{
				Analyze: tt.Analyze,
				Convert: tt.Convert,
				Match:   tt.Match,
			}

			buf := new(bytes.Buffer)
			app := newRunCmd(cfg, buf)
			err := app.Run([]string{"analyzer", "run"})
			gotErr := err != nil
			if gotErr && tt.wantErr != gotErr {
				t.Errorf("Wrong err output. Expected %v but received %v", tt.wantErr, gotErr)
			}

			got := strings.TrimSpace(buf.String())

			if tt.wantLogLevel != "" && strings.Index(got, tt.wantLogLevel) == -1 {
				t.Errorf("Wrong output. Expected '%s' to contain '%s'", got, tt.wantLogLevel)
			}

			if tt.wantDetecting != "" && strings.Index(got, tt.wantDetecting) == -1 {
				t.Errorf("Wrong output. Expected '%s' to contain '%s'", got, tt.wantDetecting)
			}

			if tt.wantFound != "" && strings.Index(got, tt.wantFound) == -1 {
				t.Errorf("Wrong output. Expected '%s' to contain '%s'", got, tt.wantFound)
			}

			if tt.wantRunning != "" && strings.Index(got, tt.wantRunning) == -1 {
				t.Errorf("Wrong output. Expected '%s' to contain '%s'", got, tt.wantRunning)
			}

			if tt.wantCreating != "" && strings.Index(got, tt.wantCreating) == -1 {
				t.Errorf("Wrong output. Expected '%s' to contain '%s'", got, tt.wantCreating)
			}
		})
	}
}

func TestRunAnalyzeSerializeJsonToFileErr(t *testing.T) {
	serializer := SerializerFunc(func(report *report.Report, artifactPath string, flagPrependPath string, indent bool, optimize bool) error {
		// Ensure artifactPath is a full path to artifact and not just artifact name
		want := "/" + ArtifactNameSAST

		if strings.Index(artifactPath, want) == -1 {
			t.Errorf("Wrong output. Expected '%s' to equal '%s'", artifactPath, want)
		}

		return nil
	})

	cfg := Config{
		Analyze:    newAnalyzeFunc(nil),
		Convert:    newConvertFunc(nil),
		Match:      newMatchFunc(nil),
		Serializer: serializer,
	}

	buf := new(bytes.Buffer)
	app := newRunCmd(cfg, buf)

	err := app.Run([]string{"analyzer", "run"})
	if err != nil {
		t.Fatal(err)
	}
}
