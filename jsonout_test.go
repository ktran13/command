package command

import (
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestSerialize(t *testing.T) {
	// test cases
	tcs := []struct {
		name        string
		indented    bool
		expectation string
	}{
		{
			"Indented",
			true,
			`{
  "version": "0.0.0",
  "vulnerabilities": null,
  "scan": {
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}
`,
		},
		{
			"Not Indented",
			false,
			`{"version":"0.0.0","vulnerabilities":null,"scan":{"scanner":{"id":"","name":"","vendor":{"name":""},"version":""},"type":""}}
`,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := report.Report{}

			t.Run("SerializeJsonToFile", func(t *testing.T) {
				jsonfile, err := ioutil.TempFile("/tmp", "jsonfile")
				require.NoError(t, err)

				err = SerializeJsonToFile(&r, jsonfile.Name(), "testdata/search", tc.indented, false)
				require.NoError(t, err, "cannot serialize JSON")

				content, err := ioutil.ReadFile(jsonfile.Name())
				require.NoError(t, err)

				require.Equal(t, tc.expectation, string(content))
			})

			t.Run("SerializeJsonToWriter", func(t *testing.T) {
				writer := new(strings.Builder)

				err := SerializeJsonToWriter(&r, writer, "testdata/search", tc.indented, false)
				require.NoError(t, err, "cannot serialize JSON")

				require.Equal(t, tc.expectation, writer.String())
			})
		})
	}
}

func TestEliminateRedundancies(t *testing.T) {
	testinput := &report.Report{
		Version: report.Version{},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 1000,
					LineEnd:   0,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
					LineEnd:   4,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
					LineEnd:   5,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/not-existent",
					LineStart: 4,
					LineEnd:   5,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
		},
		Remediations:    []report.Remediation{},
		DependencyFiles: []report.DependencyFile{},
		Scan:            report.Scan{},
		Analyzer:        "",
		Config:          ruleset.Config{},
	}

	expectation := &report.Report{
		Version: report.Version{},
		Vulnerabilities: []report.Vulnerability{
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 1000,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
			{
				Category:             "",
				Name:                 "",
				Message:              "",
				Description:          "",
				CompareKey:           "",
				Severity:             report.SeverityLevelCritical,
				Confidence:           report.ConfidenceLevelHigh,
				Solution:             "",
				RawSourceCodeExtract: "",
				Scanner:              report.Scanner{},
				Location: report.Location{
					File:      "testdata/search/b/vendor/main.c",
					LineStart: 4,
					LineEnd:   5,
				},
				Identifiers: []report.Identifier{},
				Links:       []report.Link{},
			},
		},
		Remediations:    []report.Remediation{},
		DependencyFiles: []report.DependencyFile{},
		Scan:            report.Scan{},
		Analyzer:        "",
		Config:          ruleset.Config{},
	}

	optimized, err := EliminateRedundancies(testinput, "testdata/search")
	require.NoError(t, err, "cannot run optimizations")
	require.Equal(t, optimized, expectation)
}
