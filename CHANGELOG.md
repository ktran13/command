# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases).

## v1.5.1
- Fix run to write reports to the target dir (!20)

## v1.5.0
- Add `ArtifactNameClusterImageScanning` for Cluster Image Scanning reports (!18)

## v1.4.1
- Fix location line condition (!16)
- Bump report version to v3.3.0 (!15)

## v1.4.0
- JSON artifacts generation optimizations (!14)
  - avoid redundant `report.Vulnerability.Location.LineEnd`
  - don't report vulnerabilities affecting files that are not in the git repository
  - optimizations are enabled by default and are disabled when the environment variable `ANALYZER_OPTIMIZE_REPORT` is set to `0`

## v1.3.0
- Do not indent JSON artifacts for convert command per default (!13)

## v1.2.0
- Do not indent JSON artifacts for run command per default (!12)

## v1.1.3
- Fixed vulnerabilities found by gosec (!10)

## v1.1.2
- Fixed an error that occured when rulesets were not configured (!8)

## v1.1.1
### Changed

- Clarify in log output that all directories are scanned by analyzers with `analyzeAll` enabled (!6)

## v1.1.0
### Changed

- Update subcommands to rely on `analyzers/report/v2`, switching `Issue` struct to `Vulnerability` (!3)

## v1.0.1
### Changed

- Safe handle command file closures (!2)

## v1.0.0
### Added

- Add command package for secure analyzer support (!1)

